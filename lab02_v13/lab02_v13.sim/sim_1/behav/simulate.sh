#!/bin/sh -f
xv_path="/home/mokona/Xilinx/Vivado/2015.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim count8tristate_testbench_behav -key {Behavioral:sim_1:Functional:count8tristate_testbench} -tclbatch count8tristate_testbench.tcl -view /home/mokona/src/automation/lab02_v13/count8tristate_testbench_behav.wcfg -log simulate.log
