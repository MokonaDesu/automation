library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity dsync_testbench is
end entity;

architecture behv of dsync_testbench is
    signal r: std_logic;
    signal s: std_logic;
    signal d: std_logic;
    signal clk_in: std_logic;
    signal q: std_logic;
    signal nq: std_logic;
    
begin
    dsync_instance: entity work.dsync(behv)
    port map(r, s, d, clk_in, q, nq);

    clk_update: process
    begin
        clk_in <= '0';
        wait for 1ns; 
        clk_in <= '1';
        wait for 1ns;   
    end process;

    stimulus: process
    begin
        r <= '1';
        s <= '1';
        d <= '1';
        wait for 2ns;
        r <= '0';
        wait for 2ns;
        r <= '1';
        wait for 2ns;
        d <= '0';
        wait for 8ns;
        s <= '0';
        wait for 2ns;
        s <= '1';
        wait for 2ns;
        d <= '1';
        wait for 2ns;
        d <= '0';
        wait for 8ns;        
    end process;
end architecture;