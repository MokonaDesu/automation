library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tasync_testbench is
end entity;

architecture behv of tasync_testbench is
    signal t: std_logic;
    signal r: std_logic;
    signal q: std_logic;
    signal nq: std_logic;
    
begin
    dsync_instance: entity work.tasync(behv)
    port map(t, r, q, nq);
    
    stimulus: process
    begin
        t <= '0';
        r <= '0';
        wait for 1ns;
        t <= '1';
        r <= '1';
        wait for 2ns;
        t <= '0';
        wait for 1ns;
        t <= '1';
        wait for 1ns;
        t <= '0';
        wait for 1ns;
        t <= '1';
        wait for 1ns;
        t <= '0';
        wait for 1ns;
        r <= '0';
        wait for 1ns;
        t <= '0';
        wait for 1ns;
        r <= '0';
        wait for 2ns;                
    end process;
end architecture;