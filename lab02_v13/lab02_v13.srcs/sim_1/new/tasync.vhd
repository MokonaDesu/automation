library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tasync is
    port (
        t, r:          in  std_logic;
        q, nq:         out std_logic
    );
end entity;

architecture behv of tasync is
signal not_t: std_logic;
signal d_int: std_logic;
signal q_out: std_logic;
signal nq_out: std_logic;

begin
    not_t <= not t;
    d_int <= nq_out;
    
    internal: entity work.dsync(behv) port map (
        r => r,
        s => '1',
        d => d_int,
        clk => not_t,
        q => q_out,
        nq => nq_out
    );
    
    q <= q_out;
    nq <= nq_out;
end architecture;