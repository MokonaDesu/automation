library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity count8tristate_testbench is
end entity;

architecture behv of count8tristate_testbench is
    signal oe, rclk, ccken, cclk, cclr:   std_logic;
    signal rco:                           std_logic;
    signal q:                             std_logic_vector(7 downto 0);
begin
    dsync_instance: entity work.count8tristate
    port map(oe, rclk, ccken, cclk, cclr, rco, q(0), q(1), q(2), q(3), q(4), q(5), q(6), q(7));
    
    clk_update: process
    begin
        cclk <= '0';
        rclk <= '0';
        wait for 1ns; 
        cclk <= '1';
        rclk <= '1';
        wait for 1ns;   
    end process;
    
    stimulus: process
    begin
        ccken <= '0';
        cclr <= '0';
        oe <= '0';
        
        wait for 3ns;
        cclr <= '1';
        
        wait for 256ns;        
              
        wait for 10ns;
        ccken <= '1';
        wait for 10ns;         
    end process;
end architecture;ww
