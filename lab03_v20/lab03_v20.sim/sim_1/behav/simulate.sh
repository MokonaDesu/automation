#!/bin/sh -f
xv_path="/home/mokona/Xilinx/Vivado/2015.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim file_tb_behav -key {Behavioral:sim_1:Functional:file_tb} -tclbatch file_tb.tcl -log simulate.log
