library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity auto_tb is
end entity;

architecture behv of etalon_tb is
    signal a: std_logic_vector (3 downto 0);
    signal b: std_logic_vector (3 downto 0);
    signal c: std_logic;
    
    signal sout_s: std_logic_vector (3 downto 0);
    signal cout_s: std_logic;
    
    signal expected: std_logic_vector (4 downto 0);
    
begin
    sequential_add: entity work.add4fc(seq_behv)
    port map(c, a(0), a(1), a(2), a(3), b(0), b(1), b(2), b(3), sout_s(0), sout_s(1), sout_s(2), sout_s(3), cout_s);
    
    
    stimulus: process
    variable expected_result : integer := 0;
    variable error : boolean := false;
    
    begin
        for c_integer in 0 to 1 loop
            for a_integer in 0 to 15 loop
                for b_integer in 0 to 15 loop
                    expected_result := a_integer + b_integer + c_integer;  
                    expected <= std_logic_vector(to_unsigned(expected_result, 5));
                    
                    c <= std_logic(to_unsigned(c_integer, 1)(0));
                    a <= std_logic_vector(to_unsigned(a_integer, 4));
                    b <= std_logic_vector(to_unsigned(b_integer, 4));
                    
                    wait for 1ns;
                    
                    if (std_logic(to_unsigned(expected_result, 5)(4)) /= cout_s) then
                        report "Error! Assertion failed for cout_s (" & std_logic'image(cout_s) & ") expected " & std_logic'image(std_logic(to_unsigned(expected_result, 5)(4)));
                        error := true;
                    end if;
                    
                    if (std_logic_vector(to_unsigned(expected_result, 5)(3 downto 0)) /= sout_s) then
                        report "Error! Assertion failed for sout_s (" & integer'image(to_integer(unsigned(sout_s))) & ") expected " & integer'image(to_integer(unsigned(to_unsigned(expected_result, 5)(3 downto 0))));
                        error := true;
                    end if;
                    
                    wait for 9ns;
                end loop;
            end loop;
        end loop;
        
        if (error) then
            report "Errors found";
        end if;
    end process;
end architecture;   