library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity etalon_tb is
end entity;

architecture behv of etalon_tb is
    signal a: std_logic_vector (3 downto 0);
    signal b: std_logic_vector (3 downto 0);
    signal c: std_logic;
    
    signal sout_s: std_logic_vector (3 downto 0);
    signal cout_s: std_logic;
    
    signal sout_p: std_logic_vector (3 downto 0);
    signal cout_p: std_logic;

begin
    sequential_add: entity work.add4fc(seq_behv)
    port map(c, a(0), a(1), a(2), a(3), b(0), b(1), b(2), b(3), sout_s(0), sout_s(1), sout_s(2), sout_s(3), cout_s);
    
    parallel_add: entity work.add4fc(parallel_behv)
    port map(c, a(0), a(1), a(2), a(3), b(0), b(1), b(2), b(3), sout_p(0), sout_p(1), sout_p(2), sout_p(3), cout_p);
        
    
    stimulus: process
    variable expected_result : integer := 0;
    variable error : boolean := false;
    
    begin
        for c_integer in 0 to 1 loop
            for a_integer in 0 to 15 loop
                for b_integer in 0 to 15 loop
                    
                    c <= std_logic(to_unsigned(c_integer, 1)(0));
                    a <= std_logic_vector(to_unsigned(a_integer, 4));
                    b <= std_logic_vector(to_unsigned(b_integer, 4));
                    
                    wait for 1ns;
                    
                    if (cout_p & sout_p /= cout_s & sout_s) then
                        report "Error! Assertion failed: Expected " & integer'image(to_integer(unsigned(cout_p & sout_p))) & " and got " & integer'image(to_integer(unsigned(cout_s & sout_s)));
                        error := true;
                    end if;
                end loop;
            end loop;
        end loop;
        
        if (error) then
            report "Errors found";
        else
            report "All good";
        end if;
        
        a <= "ZZZZ";
        b <= "ZZZZ";
        c <= 'Z';
        
        wait for 666ns;
    end process;
end architecture;   