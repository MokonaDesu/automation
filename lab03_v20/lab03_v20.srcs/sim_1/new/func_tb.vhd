library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity func_tb is
end entity;

architecture behv of func_tb is
    signal oe, rclk, ccken, cclk, cclr:   std_logic;
    signal rco:                           std_logic;
    signal q:                             std_logic_vector(7 downto 0);

    shared variable error: boolean := false;

begin
    dsync_instance: entity work.count8tristate
    port map(oe, rclk, ccken, cclk, cclr, rco, q(0), q(1), q(2), q(3), q(4), q(5), q(6), q(7));
    
    -- CLK update of the testbehch.
    clk_update: process
    begin
        wait for 1ns;
        loop
            if (error) then
                report "Errors found. Test is stopped!";
                wait for 1000ns;    
            end if;
        
            cclk <= '0';
            wait for 1ns; 
            cclk <= '1';
            wait for 1ns;
        end loop;   
    end process;
            
    -- Tests the output value of the counter
    -- Asserts that value should be incremented with a period of 2 ticks at most
    q_test: process
        variable previous_value: std_logic_vector(7 downto 0);
    begin
        rclk <= '1';
        wait for 1ns;
        rclk <= '0';
        wait for 50ns;
        -- Start the test after 50ns
        rclk <= '1';
        wait for 2ns;
        previous_value := q;
        wait for 2ns;
        if (to_integer(unsigned(previous_value)) + 1 /= to_integer(unsigned(q))) then
            report "Assert failed: value of Q is " & integer'image(to_integer(unsigned(q))) & " - expected " & integer'image(to_integer(unsigned(previous_value)) + 1);
            error := true;
        end if;
        rclk <= '0';
        wait for 30ns;
        rclk <= '1';
        wait for 1000ns;
    end process;
    
    -- Test the OE input pin
    -- Asserts that OE is able to set the High-Impedance state on the Q output pins
    oe_test: process
    begin
        oe <= '0';
        wait for 200ns;
        -- Start the test after 200 ns;
        
        oe <= '1';
        wait for 10ns;
        if (q /= "ZZZZZZZZ") then
            report "Assert failed: Non high-impedance with OE=1!";
            error := true;
        end if;
        wait for 10ns;
        oe <= '0';
        wait for 1000ns;
    end process;
    
    -- Test the CCLR pin
    -- Asserts that setting CCLR to zero asyncronously clears the value inside the output registers
    cclr_test: process
    begin
        cclr <= '0';
        wait for 1ns;
        cclr <= '1';
        
        wait for 350ns;
        -- Start the test after 350 ns;
        
        cclr <= '0';
        wait for 1ns;
        if (q /= "00000000") then
            report "Assert failed: CCLR=0 doesn't clear the output register!";
            error := true;
        end if;
        cclr <= '1';
        wait for 1000ns;
    end process;
    
    -- Test the CCKEN pin
    -- Asserts that setting CCKN to 1 disables the ticking of the counte
    ccken_test: process
        variable previous_value: std_logic_vector(7 downto 0);
    begin
        ccken <= '0';
        wait for 500ns;
        -- Start the test after 500ns
        
        previous_value := q;
        ccken <= '1';
        wait for 20ns;
        
        if (to_integer(unsigned(previous_value)) = to_integer(unsigned(q))) then
            report "Assert failed: value of Q is " & integer'image(to_integer(unsigned(q))) & " - expected " & integer'image(to_integer(unsigned(previous_value)));
            error := true;
        end if;
        
        ccken <= '0';
        wait for 1000ns;        
    end process;
end architecture;