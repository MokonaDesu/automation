library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use work.txt_util.all;
use IEEE.NUMERIC_STD.all;

entity file_tb is
end file_tb;
   
architecture read_from_file of file_tb is 
    file stimulus:  text open read_mode is "sim.dat";
    signal x:       std_logic_vector(3 downto 0);
    signal y:       std_logic_vector(3 downto 0);
    signal c:       std_logic_vector(0 downto 0);
    signal expected:std_logic_vector(4 downto 0);
    signal sout_s:  std_logic_vector(3 downto 0);
    signal cout_s:  std_logic;
    
begin
    parallel_add: entity work.add4fc(parallel_behv)
    port map(c(0), x(0), x(1), x(2), x(3), y(0), y(1), y(2), y(3), sout_s(0), sout_s(1), sout_s(2), sout_s(3), cout_s);
    
    receive_data: process
    
        variable l: line;
        variable sxy: string(4 downto 1);
        variable sc: string(1 downto 1);
        variable sr: string(5 downto 1);
        variable error: boolean := false;
        
        begin                                       
            while not endfile(stimulus) loop
                readline(stimulus, l);
                read(l, sc);
                c <= to_std_logic_vector(sc);
                            
                readline(stimulus, l);
                read(l, sxy);
                x <= to_std_logic_vector(sxy);
                
                readline(stimulus, l);
                read(l, sxy);
                y <= to_std_logic_vector(sxy);
                
                readline(stimulus, l);
                read(l, sr);
                expected <= to_std_logic_vector(sr);
                
                wait for 1ps;
                
                if (expected /= cout_s & sout_s) then
                    report "Error! Assertion failed for sout_s=" & integer'image(to_integer(unsigned(cout_s & sout_s))) & ". expected=" & integer'image(to_integer(unsigned(expected))) & ".";
                    error := true;
                end if;
                
                wait for 1ps;                          
            end loop;
            
        if (error) then
            report "Found errors!";
        else
            report "All good...";
        end if;
        
        c <= "Z";
        x <= "ZZZZ";
        y <= "ZZZZ";
        expected <= "ZZZZZ";
        
        wait for 666ns;
    end process;

end architecture;
 