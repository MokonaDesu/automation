library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity count8tristate is
    port (
        oe, rclk, ccken, cclk, cclr:            in std_logic;
        rco, qa, qb, qc, qd, qe, qf, qg, qh:    out std_logic
    ); 
end entity;

architecture behv of count8tristate is
    -- Control signals
    signal repeated_cclr:                       std_logic;
    signal not_oe:                              std_logic;
    signal not_rclk:                            std_logic;
    signal repeated_cclk:                       std_logic;
    signal repeated_ccken:                      std_logic;
    signal combined_cclk_ccken:                 std_logic;
    signal internal_clk:                        std_logic;
    
    -- Register connections
    signal tasync_a_q, tasync_a_nq, tri_out_a:  std_logic;
    signal tasync_b_q, tasync_b_nq, tri_out_b:  std_logic;
    signal tasync_c_q, tasync_c_nq, tri_out_c:  std_logic;
    signal tasync_d_q, tasync_d_nq, tri_out_d:  std_logic;
    signal tasync_e_q, tasync_e_nq, tri_out_e:  std_logic;
    signal tasync_f_q, tasync_f_nq, tri_out_f:  std_logic;
    signal tasync_g_q, tasync_g_nq, tri_out_g:  std_logic;
    signal tasync_h_q, tasync_h_nq, tri_out_h:  std_logic;
    
begin

    -- Internal clk combination is weird (11, 12)
    --internal_clk <= cclk nand ((not (not (not cclk))) and (not (not ((not ccken) nor (not internal_clk)))));    
    -- is the same as:
    --repeated_cclk <= not (not cclk);
    --repeated_ccken <= not ((not ccken) or (not internal_clk));
    --combined_cclk_ccken <= (not (repeated_cclk)) and (not (repeated_ccken));
    --internal_clk <= not (combined_cclk_ccken and cclk);
    -- Yields 1 all the time (???)
    
    internal_clk <= not cclk;    
    repeated_cclr <= not (not cclr);
    not_rclk <= not rclk;
    not_oe <= not oe;
    
    -- QA module
    tasync_a: entity work.tasync port map (
        t => internal_clk,
        r => repeated_cclr,
        q => tasync_a_q,
        nq => tasync_a_nq
    );
    
    rssync_a: entity work.rssync port map (
        r => tasync_a_nq,
        s => tasync_a_q,
        clk => not_rclk,
        nq => tri_out_a 
    );
    
    qa <= 'Z' when not_oe = '0' else (not tri_out_a);
    
    -- QB module
    tasync_b: entity work.tasync port map (
        t => tasync_a_q and internal_clk,
        r => repeated_cclr,
        q => tasync_b_q,
        nq => tasync_b_nq
    );
    
    rssync_b: entity work.rssync port map (
        r => tasync_b_nq,
        s => tasync_b_q,
        clk => not_rclk,
        nq => tri_out_b
    );
    
    qb <= 'Z' when not_oe = '0' else (not tri_out_b);
    
    -- QC module
    tasync_c: entity work.tasync port map (
        t => tasync_b_q and tasync_a_q and internal_clk,
        r => repeated_cclr,
        q => tasync_c_q,
        nq => tasync_c_nq
    );
        
    rssync_c: entity work.rssync port map (
        r => tasync_c_nq,
        s => tasync_c_q,
        clk => not_rclk,
        nq => tri_out_c
    );
        
    qc <= 'Z' when not_oe = '0' else (not tri_out_c);
    
    -- QD module
    tasync_d: entity work.tasync port map (
        t => tasync_c_q and tasync_b_q and tasync_a_q and internal_clk,
        r => repeated_cclr,
        q => tasync_d_q,
        nq => tasync_d_nq
    );
        
    rssync_d: entity work.rssync port map (
        r => tasync_d_nq,
        s => tasync_d_q,
        clk => not_rclk,
        nq => tri_out_d
    );
        
    qd <= 'Z' when not_oe = '0' else (not tri_out_d);
    
    -- QE module
    tasync_e: entity work.tasync port map (
        t => tasync_d_q and tasync_c_q and tasync_b_q and tasync_a_q and internal_clk,
        r => repeated_cclr,
        q => tasync_e_q,
        nq => tasync_e_nq
    );
        
    rssync_e: entity work.rssync port map (
        r => tasync_e_nq,
        s => tasync_e_q,
        clk => not_rclk,
        nq => tri_out_e
    );
        
    qe <= 'Z' when not_oe = '0' else (not tri_out_e);

    -- QF module
    tasync_f: entity work.tasync port map (
        t => tasync_e_q and tasync_d_q and tasync_c_q and tasync_b_q and tasync_a_q and internal_clk,
        r => repeated_cclr,
        q => tasync_f_q,
        nq => tasync_f_nq
    );
        
    rssync_f: entity work.rssync port map (
        r => tasync_f_nq,
        s => tasync_f_q,
        clk => not_rclk,
        nq => tri_out_f
    );
        
    qf <= 'Z' when not_oe = '0' else (not tri_out_f);

    -- QG module
    tasync_g: entity work.tasync port map (
        t => tasync_f_q and tasync_e_q and tasync_d_q and tasync_c_q and tasync_b_q and tasync_a_q and internal_clk,
        r => repeated_cclr,
        q => tasync_g_q,
        nq => tasync_g_nq
    );
        
    rssync_g: entity work.rssync port map (
        r => tasync_g_nq,
        s => tasync_g_q,
        clk => not_rclk,
        nq => tri_out_g
    );
        
    qg <= 'Z' when not_oe = '0' else (not tri_out_g);

    -- QH module
    tasync_h: entity work.tasync port map (
        t => tasync_g_q and tasync_f_q and tasync_e_q and tasync_d_q and tasync_c_q and tasync_b_q and tasync_a_q and internal_clk,
        r => repeated_cclr,
        q => tasync_h_q,
        nq => tasync_h_nq
    );
        
    rssync_h: entity work.rssync port map (
        r => tasync_h_nq,
        s => tasync_h_q,
        clk => not_rclk,
        nq => tri_out_h
    );
        
    qh <= 'Z' when not_oe = '0' else (not tri_out_h);

    -- RCO flag
    rco <= not (tasync_h_q and tasync_g_q and tasync_f_q and tasync_e_q and tasync_d_q and tasync_a_q and tasync_b_q and tasync_c_q);
end architecture;