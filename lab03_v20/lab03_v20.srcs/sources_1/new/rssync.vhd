library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity rssync is
    port (
        r, s, clk:     in  std_logic;
        q, nq:         out std_logic
    );
end entity;

architecture behv of rssync is

signal rclk: std_logic;
signal sclk: std_logic;

begin
    rclk <= not (r and not clk);
    sclk <= not (s and not clk);

    internal: entity work.dsync(behv) port map (
        r => rclk, 
        s => sclk,
        d => '0',
        clk => '0',
        q => q,
        nq => nq
    );         
end architecture;