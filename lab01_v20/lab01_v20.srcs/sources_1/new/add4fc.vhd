library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity add4fc is
  port (
    -- Previous carry
    c0: in std_logic;
    
    -- Input value bits
    a1, a2, a3, a4: in std_logic;   
    b1, b2, b3, b4: in std_logic;
    
    -- Results
    s1, s2, s3, s4: out std_logic;
    
    -- Next carry
    c4: out std_logic
  );
end entity;

architecture parallel_behv of add4fc is
    -- Partial sums
    signal ns1, ns2, ns3, ns4: std_logic;
    
    -- Partial carries
    signal nc0, nc1, nc2, nc3, nc4: std_logic;
    
begin
    nc0 <= (not c0);
    ns1 <= (a1 nor b1);
    nc1 <= (a1 nand b1); 
    ns2 <= (a2 nor b2);
    nc2 <= (a2 nand b2);
    ns3 <= (a3 nor b3);
    nc3 <= (a3 nand b3);
    ns4 <= (a4 nor b4);
    nc4 <= (a4 nand b4);
    
    s1 <= (not nc0) xor ((not ns1) and nc1);
    s2 <= ((nc1 and nc0) nor ns1) xor ((not ns2) and nc2); 
    s3 <= (not ((nc0 and nc1 and nc2) or (nc2 and ns1) or ns2)) xor ((not ns3) and nc3);
    s4 <= (not ((nc0 and nc1 and nc2 and nc3) or (nc3 and nc2 and ns1) or (nc3 and ns2) or ns3)) xor ((not ns4) and nc4);
    c4 <= (not ((nc0 and nc1 and nc2 and nc3 and nc4) or (nc4 and nc3 and nc2 and ns1) or (nc4 and nc3 and ns2) or (nc4 and ns3) or ns4));  
end architecture;

architecture seq_behv of add4fc is
    -- Partial sums
    signal ns1, ns2, ns3, ns4: std_logic;
    
    -- Partial carries
    signal nc0, nc1, nc2, nc3, nc4: std_logic;
begin
    update_first_layer: process(c0, a1, a2, a3, a4, b1, b2, b3, b4)
    begin
        nc0 <= (not c0);
        ns1 <= (a1 nor b1);
        nc1 <= (a1 nand b1); 
        ns2 <= (a2 nor b2);
        nc2 <= (a2 nand b2);
        ns3 <= (a3 nor b3);
        nc3 <= (a3 nand b3);
        ns4 <= (a4 nor b4);
        nc4 <= (a4 nand b4);
    end process;

    update_inner_layer: process(nc0, ns1, nc1, ns2, nc2, ns3, nc3, ns4, nc4)
    begin
        s1 <= (not nc0) xor ((not ns1) and nc1);
        s2 <= ((nc1 and nc0) nor ns1) xor ((not ns2) and nc2); 
        s3 <= (not ((nc0 and nc1 and nc2) or (nc2 and ns1) or ns2)) xor ((not ns3) and nc3);
        s4 <= (not ((nc0 and nc1 and nc2 and nc3) or (nc3 and nc2 and ns1) or (nc3 and ns2) or ns3)) xor ((not ns4) and nc4);
        c4 <= (not ((nc0 and nc1 and nc2 and nc3 and nc4) or (nc4 and nc3 and nc2 and ns1) or (nc4 and nc3 and ns2) or (nc4 and ns3) or ns4));  
    end process;
end architecture;
l
