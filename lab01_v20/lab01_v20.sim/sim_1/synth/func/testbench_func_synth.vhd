-- Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2015.2 (lin64) Build 1266856 Fri Jun 26 16:35:25 MDT 2015
-- Date        : Fri Sep 16 15:35:09 2016
-- Host        : notmac running 64-bit elementary OS Freya
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/mokona/src/automation/lab01_v20/lab01_v20.sim/sim_1/synth/func/testbench_func_synth.vhd
-- Design      : add4fc
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tfbg676-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity add4fc is
  port (
    c0 : in STD_LOGIC;
    a1 : in STD_LOGIC;
    a2 : in STD_LOGIC;
    a3 : in STD_LOGIC;
    a4 : in STD_LOGIC;
    b1 : in STD_LOGIC;
    b2 : in STD_LOGIC;
    b3 : in STD_LOGIC;
    b4 : in STD_LOGIC;
    s1 : out STD_LOGIC;
    s2 : out STD_LOGIC;
    s3 : out STD_LOGIC;
    s4 : out STD_LOGIC;
    c4 : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of add4fc : entity is true;
end add4fc;

architecture STRUCTURE of add4fc is
  signal a1_IBUF : STD_LOGIC;
  signal a2_IBUF : STD_LOGIC;
  signal a3_IBUF : STD_LOGIC;
  signal a4_IBUF : STD_LOGIC;
  signal b1_IBUF : STD_LOGIC;
  signal b2_IBUF : STD_LOGIC;
  signal b3_IBUF : STD_LOGIC;
  signal b4_IBUF : STD_LOGIC;
  signal c0_IBUF : STD_LOGIC;
  signal c4_OBUF : STD_LOGIC;
  signal s1_OBUF : STD_LOGIC;
  signal s2_OBUF : STD_LOGIC;
  signal s3_OBUF : STD_LOGIC;
  signal s3_OBUF_inst_i_2_n_0 : STD_LOGIC;
  signal s3_OBUF_inst_i_3_n_0 : STD_LOGIC;
  signal s4_OBUF : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of s1_OBUF_inst_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of s2_OBUF_inst_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of s3_OBUF_inst_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of s3_OBUF_inst_i_3 : label is "soft_lutpair1";
begin
a1_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => a1,
      O => a1_IBUF
    );
a2_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => a2,
      O => a2_IBUF
    );
a3_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => a3,
      O => a3_IBUF
    );
a4_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => a4,
      O => a4_IBUF
    );
b1_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => b1,
      O => b1_IBUF
    );
b2_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => b2,
      O => b2_IBUF
    );
b3_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => b3,
      O => b3_IBUF
    );
b4_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => b4,
      O => b4_IBUF
    );
c0_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => c0,
      O => c0_IBUF
    );
c4_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => c4_OBUF,
      O => c4
    );
c4_OBUF_inst_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => s3_OBUF_inst_i_2_n_0,
      I1 => a3_IBUF,
      I2 => b3_IBUF,
      I3 => b4_IBUF,
      I4 => a4_IBUF,
      O => c4_OBUF
    );
s1_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => s1_OBUF,
      O => s1
    );
s1_OBUF_inst_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => c0_IBUF,
      I1 => a1_IBUF,
      I2 => b1_IBUF,
      O => s1_OBUF
    );
s2_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => s2_OBUF,
      O => s2
    );
s2_OBUF_inst_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99969666"
    )
        port map (
      I0 => a2_IBUF,
      I1 => b2_IBUF,
      I2 => b1_IBUF,
      I3 => a1_IBUF,
      I4 => c0_IBUF,
      O => s2_OBUF
    );
s3_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => s3_OBUF,
      O => s3
    );
s3_OBUF_inst_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BE82"
    )
        port map (
      I0 => s3_OBUF_inst_i_2_n_0,
      I1 => a3_IBUF,
      I2 => b3_IBUF,
      I3 => s3_OBUF_inst_i_3_n_0,
      O => s3_OBUF
    );
s3_OBUF_inst_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFE8E800"
    )
        port map (
      I0 => c0_IBUF,
      I1 => a1_IBUF,
      I2 => b1_IBUF,
      I3 => a2_IBUF,
      I4 => b2_IBUF,
      O => s3_OBUF_inst_i_2_n_0
    );
s3_OBUF_inst_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"001717FF"
    )
        port map (
      I0 => c0_IBUF,
      I1 => a1_IBUF,
      I2 => b1_IBUF,
      I3 => a2_IBUF,
      I4 => b2_IBUF,
      O => s3_OBUF_inst_i_3_n_0
    );
s4_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => s4_OBUF,
      O => s4
    );
s4_OBUF_inst_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3C3EB28EB283C3C"
    )
        port map (
      I0 => s3_OBUF_inst_i_3_n_0,
      I1 => b4_IBUF,
      I2 => a4_IBUF,
      I3 => s3_OBUF_inst_i_2_n_0,
      I4 => a3_IBUF,
      I5 => b3_IBUF,
      O => s4_OBUF
    );
end STRUCTURE;
