#!/bin/sh -f
xv_path="/home/mokona/Xilinx/Vivado/2015.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto 4ac2d76bb0434229a03842075cda320d -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot testbench_func_synth xil_defaultlib.testbench -log elaborate.log
