library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity tasync_tb is
end entity;

architecture behv of tasync_tb is
    signal t, r, q: std_logic;
begin

    tested: entity work.tasync(behv) port map (
        t, r, q
    );
    
process
begin
    t <= '1';
    r <= '1';
    
    wait for 10ns;
    r <= '0';
    wait for 1ns;
    r <= '1';
    t <= '0';
    wait for 1ns;
    t <= '1';
    wait for 1ns;
    t <= '0';
    wait for 1ns;
    t <= '1';
    wait for 1ns;
    t <= '0';
    wait for 1ns;
    t <= '1';
    wait for 1ns;
    t <= '0';
    wait for 1ns;
    t <= '1';
    wait for 1ns;
 
    r <= '0';

end process;
end architecture;