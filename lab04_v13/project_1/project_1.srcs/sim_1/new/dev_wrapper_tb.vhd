library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity dev_wrapper_tb is
end entity;

architecture behv of dev_wrapper_tb is
    component dev_wrapper
        port (
            -- CLK Diff pair 
            sysclk_p:   in std_logic;
            sysclk_n:   in std_logic;
        
            -- Buttons
            btn_oe:     in std_logic;
            btn_rclk:   in std_logic;
            btn_ccken:  in std_logic;
            btn_cclr:   in std_logic;
        
            -- Output LEDs
            led_q:      out std_logic_vector(7 downto 0);
            led_rco:    out std_logic    
        );
    end component;

    signal clk_p:  std_logic;
    signal clk_n:  std_logic;
    
    signal oe:     std_logic;
    signal rclk:   std_logic;
    signal ccken:  std_logic;
    signal cclr:   std_logic;
    
    signal q:      std_logic_vector(7 downto 0);
    signal rco:    std_logic;
    
    constant micro_tick: time := 5ns;
    constant user_tick: time := 8 * 256 * 256 * 256 * micro_tick;
    
begin
    
    tested_dev: dev_wrapper port map (
        sysclk_p    => clk_p,
        sysclk_n    => clk_n,
        btn_oe      => oe,
        btn_rclk    => rclk,
        btn_ccken   => ccken,
        btn_cclr    => cclr,
        led_q       => q,
        led_rco     => rco
    );
    
    clk_n <= not clk_p;

     process
        variable mode:  integer range 0 to 2 := 0;
        variable subtick: integer;
     begin
        -- Foreach of the modes (0 = clear, 1 = count up, 2 = propagate)
        for mode in 0 to 2 loop
            -- Logic clear
                cclr <= '1';
                oe   <= '0';
                rclk <= '1';
                ccken <= '0';
            wait for user_tick;
            
            case mode is
                -- Test clear mode
                when 0 => 
                    cclr <= '1';
                    wait for micro_tick;
                    cclr <= '0';
                    wait for micro_tick;                
                -- Test count up mode
                when 1 =>
                    for subtick in 0 to 16*256*256*256 loop
                        clk_p <= '1';
                        wait for micro_tick;
                        clk_p <= '0';
                        wait for micro_tick;
                    end loop;
                                    
                    wait for user_tick;
                    
                -- Test propagation mode
                when 2 =>
                    wait for user_tick;
            end case;
        end loop;
        mode := 0;
    end process; 
end architecture;