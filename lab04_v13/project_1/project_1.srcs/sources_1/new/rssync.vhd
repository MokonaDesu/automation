library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity rssync is
    port (
        r, s, clk:     in  std_logic;
        nq:            out std_logic
    );
end entity;

architecture behv of rssync is
begin
    process(r, s, clk)
    begin
        if falling_edge(clk) then
            if r = '1' then
                nq <= '1';
            elsif s = '1' then
                nq <= '0';
            else
                nq <= 'U';
            end if;
        end if;
    end process;
end architecture;