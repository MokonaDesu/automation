library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity dev_wrapper is
    port (
        -- CLK Diff pair 
        sysclk_p:   in std_logic;
        sysclk_n:   in std_logic;
        
        -- Buttons
        btn_oe:     in std_logic;
        btn_rclk:   in std_logic;
        btn_ccken:  in std_logic;
        btn_cclr:   in std_logic;
        
        -- Output LEDs
        led_q:      out std_logic_vector(7 downto 0);
        led_rco:    out std_logic
    );
end entity;

architecture behv of dev_wrapper is
    -- Working dev component
    component count8tristate
        port (
            oe, rclk, ccken, cclk, cclr:            in std_logic;
            rco, qa, qb, qc, qd, qe, qf, qg, qh:    out std_logic
        );
    end component;

    -- Frequency dividers for CLK
    component freqdiv_256
        port (
            clkin:  in std_logic;
            reset:  in std_logic;
            clkout: out std_logic
        );
    end component;
    
    -- Frequency dividers for CLK
    component freqdiv_8
        port (
            clkin:  in std_logic;
            reset:  in std_logic;
            clkout: out std_logic
        );
    end component;
        
    
    -- CLK before frequency division
    signal prediv_clk:  std_logic;
    
    -- Usable CLK after frequency divison
    signal postdiv_clk, postdiv_clk_1, postdiv_clk_2, postdiv_clk_3: std_logic;
    
    -- Inverted buttons
    signal n_btn_rclk, n_btn_ccken, n_btn_cclr: std_logic;
begin

-- Invert me the buttons
n_btn_rclk <= btn_rclk and postdiv_clk;
n_btn_ccken <= not btn_ccken;
n_btn_cclr <= not btn_cclr;

-- Diff pair to clk (high freq)
clk_ibufds: IBUFDS generic map (
    IOSTANDARD => "DEFAULT"
) port map (
    I       => sysclk_p,
    IB      => sysclk_n,
    O       => prediv_clk
);   

clk_div_256: freqdiv_256 port map (
    clkin   => prediv_clk,
    reset   => n_btn_cclr,
    clkout  => postdiv_clk_1
);

clk_div_65536: freqdiv_256 port map (
    clkin   => postdiv_clk_1,
    reset   => n_btn_cclr,
    clkout  => postdiv_clk_2
);

clk_div_16777216: freqdiv_256 port map (
    clkin   => postdiv_clk_2,
    reset   => n_btn_cclr,
    clkout  => postdiv_clk_3
);

final_clk_div: freqdiv_8 port map (
    clkin   => postdiv_clk_3,
    reset   => n_btn_cclr,
    clkout  => postdiv_clk
);

counter: count8tristate port map (
    -- Inputs
    oe      => btn_oe,
    rclk    => n_btn_rclk,
    ccken   => n_btn_ccken,
    cclk    => postdiv_clk,
    cclr    => btn_cclr,
    
    -- Outputs
    qa      => led_q(0),
    qb      => led_q(1),
    qc      => led_q(2),
    qd      => led_q(3),
    qe      => led_q(4),
    qf      => led_q(5),
    qg      => led_q(6),
    qh      => led_q(7),
    rco     => led_rco
);

end architecture;