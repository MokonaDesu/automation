library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tasync is
    port (
        t, r:          in  std_logic;
        q, nq:         out std_logic
    );
end entity;

architecture behv of tasync is
    signal temp: std_logic;
begin
    process(t, r)
    begin
    if r = '0' then
        temp <= '0';
    elsif falling_edge(t) then
        temp <= not temp;
    end if;
    end process;
    
    q <= temp;
    nq <= not temp;
end architecture;