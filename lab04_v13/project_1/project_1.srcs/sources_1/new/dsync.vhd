library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity dsync is
    port (
        r, s, d, clk:     in  std_logic;
        q, nq:            out std_logic
    );
end entity;

architecture behv of dsync is
begin
    process(s, r, clk)
    begin
        if r = '0' then
            q <= '0';
            nq <= '1';
        elsif s = '0' then
            q <= '1';
            nq <= '0';
        elsif (clk'event and clk = '1') then
            q <= d;
            nq <= not d;
        end if;
    end process;
end architecture;