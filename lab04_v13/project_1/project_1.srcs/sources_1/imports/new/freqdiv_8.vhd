library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity freqdiv_8 is
    port (
        clkin:  in std_logic;
        reset:  in std_logic;
        clkout: out std_logic
    );
end entity;

architecture behv of freqdiv_8 is
    signal div_2, div_4: std_logic;
    signal n_reset: std_logic;

begin
    n_reset <= not reset;

    tasync_2: entity work.tasync port map (
        t => clkin,
        r => n_reset,
        q => div_2
    );
    
    tasync_4: entity work.tasync port map (
        t => div_2,
        r => n_reset,
        q => div_4
    );

    tasync_8: entity work.tasync port map (
        t => div_4,
        r => n_reset,
        q => clkout
    );
    
end architecture;
