#!/bin/sh -f
xv_path="/home/mokona/Xilinx/Vivado/2015.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto 15d1920627c64b0c9ccaca976e9cb17a -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot dev_wrapper_tb_func_impl xil_defaultlib.dev_wrapper_tb -log elaborate.log
