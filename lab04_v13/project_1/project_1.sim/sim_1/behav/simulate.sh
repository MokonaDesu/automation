#!/bin/sh -f
xv_path="/home/mokona/Xilinx/Vivado/2015.2"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xsim dev_wrapper_tb_behav -key {Behavioral:sim_1:Functional:dev_wrapper_tb} -tclbatch dev_wrapper_tb.tcl -view /home/mokona/src/automation/lab04_v13/project_1/dev_wrapper_tb_func_impl.wcfg -log simulate.log
