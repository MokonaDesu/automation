-- Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2015.2 (lin64) Build 1266856 Fri Jun 26 16:35:25 MDT 2015
-- Date        : Thu Dec  8 17:53:07 2016
-- Host        : notmac running 64-bit elementary OS Freya
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/mokona/src/automation/lab04_v13/project_1/project_1.sim/sim_1/synth/func/dev_wrapper_tb_func_synth.vhd
-- Design      : dev_wrapper
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tfbg676-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rssync is
  port (
    led_q_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    tasync_a_nq : in STD_LOGIC;
    not_rclk : in STD_LOGIC
  );
end rssync;

architecture STRUCTURE of rssync is
  signal tri_out_a : STD_LOGIC;
begin
\led_q_OBUFT[0]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tri_out_a,
      O => led_q_OBUF(0)
    );
nq_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => not_rclk,
      CE => '1',
      D => tasync_a_nq,
      Q => tri_out_a,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rssync_0 is
  port (
    led_q_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    tasync_b_nq : in STD_LOGIC;
    not_rclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of rssync_0 : entity is "rssync";
end rssync_0;

architecture STRUCTURE of rssync_0 is
  signal tri_out_b : STD_LOGIC;
begin
\led_q_OBUFT[1]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tri_out_b,
      O => led_q_OBUF(0)
    );
nq_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => not_rclk,
      CE => '1',
      D => tasync_b_nq,
      Q => tri_out_b,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rssync_1 is
  port (
    led_q_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    tasync_c_nq : in STD_LOGIC;
    not_rclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of rssync_1 : entity is "rssync";
end rssync_1;

architecture STRUCTURE of rssync_1 is
  signal tri_out_c : STD_LOGIC;
begin
\led_q_OBUFT[2]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tri_out_c,
      O => led_q_OBUF(0)
    );
nq_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => not_rclk,
      CE => '1',
      D => tasync_c_nq,
      Q => tri_out_c,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rssync_2 is
  port (
    led_q_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    tasync_d_nq : in STD_LOGIC;
    not_rclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of rssync_2 : entity is "rssync";
end rssync_2;

architecture STRUCTURE of rssync_2 is
  signal tri_out_d : STD_LOGIC;
begin
\led_q_OBUFT[3]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tri_out_d,
      O => led_q_OBUF(0)
    );
nq_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => not_rclk,
      CE => '1',
      D => tasync_d_nq,
      Q => tri_out_d,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rssync_3 is
  port (
    led_q_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    tasync_e_nq : in STD_LOGIC;
    not_rclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of rssync_3 : entity is "rssync";
end rssync_3;

architecture STRUCTURE of rssync_3 is
  signal tri_out_e : STD_LOGIC;
begin
\led_q_OBUFT[4]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tri_out_e,
      O => led_q_OBUF(0)
    );
nq_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => not_rclk,
      CE => '1',
      D => tasync_e_nq,
      Q => tri_out_e,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rssync_4 is
  port (
    led_q_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    tasync_f_nq : in STD_LOGIC;
    not_rclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of rssync_4 : entity is "rssync";
end rssync_4;

architecture STRUCTURE of rssync_4 is
  signal tri_out_f : STD_LOGIC;
begin
\led_q_OBUFT[5]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tri_out_f,
      O => led_q_OBUF(0)
    );
nq_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => not_rclk,
      CE => '1',
      D => tasync_f_nq,
      Q => tri_out_f,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rssync_5 is
  port (
    led_q_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    tasync_g_nq : in STD_LOGIC;
    not_rclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of rssync_5 : entity is "rssync";
end rssync_5;

architecture STRUCTURE of rssync_5 is
  signal tri_out_g : STD_LOGIC;
begin
\led_q_OBUFT[6]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tri_out_g,
      O => led_q_OBUF(0)
    );
nq_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => not_rclk,
      CE => '1',
      D => tasync_g_nq,
      Q => tri_out_g,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rssync_6 is
  port (
    led_q_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    tasync_h_nq : in STD_LOGIC;
    not_rclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of rssync_6 : entity is "rssync";
end rssync_6;

architecture STRUCTURE of rssync_6 is
  signal tri_out_h : STD_LOGIC;
begin
\led_q_OBUFT[7]_inst_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => tri_out_h,
      O => led_q_OBUF(0)
    );
nq_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => not_rclk,
      CE => '1',
      D => tasync_h_nq,
      Q => tri_out_h,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync is
  port (
    tasync_a_q : out STD_LOGIC;
    tasync_a_nq : out STD_LOGIC;
    tasync_b_t : out STD_LOGIC;
    tasync_c_t : out STD_LOGIC;
    tasync_e_t : out STD_LOGIC;
    led_rco : out STD_LOGIC;
    internal_clk : in STD_LOGIC;
    btn_cclr : in STD_LOGIC;
    cclk : in STD_LOGIC;
    tasync_b_q : in STD_LOGIC;
    tasync_c_q : in STD_LOGIC;
    tasync_d_q : in STD_LOGIC;
    lopt : in STD_LOGIC
  );
end tasync;

architecture STRUCTURE of tasync is
  signal \^tasync_a_nq\ : STD_LOGIC;
  signal \^tasync_a_q\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of temp_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of temp_i_2 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \temp_i_2__1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \temp_i_2__3\ : label is "soft_lutpair0";
begin
  tasync_a_nq <= \^tasync_a_nq\;
  tasync_a_q <= \^tasync_a_q\;
led_rco_OBUF_inst_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^tasync_a_q\,
      I1 => tasync_b_q,
      O => led_rco
    );
temp_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tasync_a_q\,
      O => \^tasync_a_nq\
    );
temp_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^tasync_a_q\,
      I1 => cclk,
      O => tasync_b_t
    );
\temp_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^tasync_a_q\,
      I1 => tasync_b_q,
      I2 => cclk,
      O => tasync_c_t
    );
\temp_i_2__3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00008000"
    )
        port map (
      I0 => \^tasync_a_q\,
      I1 => tasync_b_q,
      I2 => tasync_c_q,
      I3 => tasync_d_q,
      I4 => cclk,
      O => tasync_e_t
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '0'
    )
        port map (
      C => lopt,
      CE => '1',
      CLR => btn_cclr,
      D => \^tasync_a_nq\,
      Q => \^tasync_a_q\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_10 is
  port (
    tasync_e_q : out STD_LOGIC;
    tasync_e_nq : out STD_LOGIC;
    tasync_f_t : out STD_LOGIC;
    tasync_e_t : in STD_LOGIC;
    btn_cclr : in STD_LOGIC;
    tasync_d_q : in STD_LOGIC;
    tasync_c_q : in STD_LOGIC;
    tasync_b_q : in STD_LOGIC;
    tasync_a_q : in STD_LOGIC;
    cclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_10 : entity is "tasync";
end tasync_10;

architecture STRUCTURE of tasync_10 is
  signal \^tasync_e_nq\ : STD_LOGIC;
  signal \^tasync_e_q\ : STD_LOGIC;
begin
  tasync_e_nq <= \^tasync_e_nq\;
  tasync_e_q <= \^tasync_e_q\;
\temp_i_1__3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tasync_e_q\,
      O => \^tasync_e_nq\
    );
\temp_i_2__2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \^tasync_e_q\,
      I1 => tasync_d_q,
      I2 => tasync_c_q,
      I3 => tasync_b_q,
      I4 => tasync_a_q,
      I5 => cclk,
      O => tasync_f_t
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => tasync_e_t,
      CE => '1',
      CLR => btn_cclr,
      D => \^tasync_e_nq\,
      Q => \^tasync_e_q\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_11 is
  port (
    tasync_f_q : out STD_LOGIC;
    tasync_f_nq : out STD_LOGIC;
    led_rco_OBUF : out STD_LOGIC;
    tasync_f_t : in STD_LOGIC;
    btn_cclr : in STD_LOGIC;
    tasync_e_q : in STD_LOGIC;
    temp_reg_0 : in STD_LOGIC;
    temp_reg_1 : in STD_LOGIC;
    tasync_h_q : in STD_LOGIC;
    tasync_g_q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_11 : entity is "tasync";
end tasync_11;

architecture STRUCTURE of tasync_11 is
  signal \^tasync_f_nq\ : STD_LOGIC;
  signal \^tasync_f_q\ : STD_LOGIC;
begin
  tasync_f_nq <= \^tasync_f_nq\;
  tasync_f_q <= \^tasync_f_q\;
led_rco_OBUF_inst_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF7FFFFFFFFFFFF"
    )
        port map (
      I0 => \^tasync_f_q\,
      I1 => tasync_e_q,
      I2 => temp_reg_0,
      I3 => temp_reg_1,
      I4 => tasync_h_q,
      I5 => tasync_g_q,
      O => led_rco_OBUF
    );
\temp_i_1__4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tasync_f_q\,
      O => \^tasync_f_nq\
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => tasync_f_t,
      CE => '1',
      CLR => btn_cclr,
      D => \^tasync_f_nq\,
      Q => \^tasync_f_q\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_12 is
  port (
    tasync_g_q : out STD_LOGIC;
    tasync_g_nq : out STD_LOGIC;
    temp_reg_0 : out STD_LOGIC;
    tasync_h_t : out STD_LOGIC;
    tasync_g_t : in STD_LOGIC;
    cclk : in STD_LOGIC;
    temp_reg_1 : in STD_LOGIC;
    btn_cclr_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_12 : entity is "tasync";
end tasync_12;

architecture STRUCTURE of tasync_12 is
  signal \^tasync_g_nq\ : STD_LOGIC;
  signal \^tasync_g_q\ : STD_LOGIC;
  signal \^temp_reg_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \temp_i_1__5\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \temp_i_2__5\ : label is "soft_lutpair3";
begin
  tasync_g_nq <= \^tasync_g_nq\;
  tasync_g_q <= \^tasync_g_q\;
  temp_reg_0 <= \^temp_reg_0\;
\temp_i_1__5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tasync_g_q\,
      O => \^tasync_g_nq\
    );
\temp_i_2__5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => cclk,
      I1 => \^tasync_g_q\,
      I2 => temp_reg_1,
      O => tasync_h_t
    );
\temp_i_2__6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => btn_cclr_IBUF,
      O => \^temp_reg_0\
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => tasync_g_t,
      CE => '1',
      CLR => \^temp_reg_0\,
      D => \^tasync_g_nq\,
      Q => \^tasync_g_q\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_13 is
  port (
    tasync_h_q : out STD_LOGIC;
    tasync_h_nq : out STD_LOGIC;
    tasync_h_t : in STD_LOGIC;
    btn_cclr : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_13 : entity is "tasync";
end tasync_13;

architecture STRUCTURE of tasync_13 is
  signal \^tasync_h_nq\ : STD_LOGIC;
  signal \^tasync_h_q\ : STD_LOGIC;
begin
  tasync_h_nq <= \^tasync_h_nq\;
  tasync_h_q <= \^tasync_h_q\;
\temp_i_1__6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tasync_h_q\,
      O => \^tasync_h_nq\
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => tasync_h_t,
      CE => '1',
      CLR => btn_cclr,
      D => \^tasync_h_nq\,
      Q => \^tasync_h_q\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_14 is
  port (
    div_2 : out STD_LOGIC;
    prediv_clk_BUFG : in STD_LOGIC;
    btn_cclr : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_14 : entity is "tasync";
end tasync_14;

architecture STRUCTURE of tasync_14 is
  signal \^div_2\ : STD_LOGIC;
  signal \temp_i_1__7_n_0\ : STD_LOGIC;
begin
  div_2 <= \^div_2\;
\temp_i_1__7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^div_2\,
      O => \temp_i_1__7_n_0\
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => prediv_clk_BUFG,
      CE => '1',
      CLR => btn_cclr,
      D => \temp_i_1__7_n_0\,
      Q => \^div_2\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_15 is
  port (
    div_4 : out STD_LOGIC;
    div_2 : in STD_LOGIC;
    btn_cclr : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_15 : entity is "tasync";
end tasync_15;

architecture STRUCTURE of tasync_15 is
  signal \^div_4\ : STD_LOGIC;
  signal \temp_i_1__8_n_0\ : STD_LOGIC;
begin
  div_4 <= \^div_4\;
\temp_i_1__8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^div_4\,
      O => \temp_i_1__8_n_0\
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => div_2,
      CE => '1',
      CLR => btn_cclr,
      D => \temp_i_1__8_n_0\,
      Q => \^div_4\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_16 is
  port (
    cclk : out STD_LOGIC;
    not_rclk : out STD_LOGIC;
    internal_clk : in STD_LOGIC;
    div_4 : in STD_LOGIC;
    btn_cclr : in STD_LOGIC;
    btn_rclk_IBUF : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_16 : entity is "tasync";
end tasync_16;

architecture STRUCTURE of tasync_16 is
  signal \^cclk\ : STD_LOGIC;
begin
  cclk <= \^cclk\;
nq_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^cclk\,
      I1 => btn_rclk_IBUF,
      O => not_rclk
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => div_4,
      CE => '1',
      CLR => btn_cclr,
      D => internal_clk,
      Q => \^cclk\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_7 is
  port (
    tasync_b_q : out STD_LOGIC;
    tasync_b_nq : out STD_LOGIC;
    temp_reg_0 : out STD_LOGIC;
    tasync_b_t : in STD_LOGIC;
    btn_cclr : in STD_LOGIC;
    tasync_a_q : in STD_LOGIC;
    tasync_e_q : in STD_LOGIC;
    tasync_f_q : in STD_LOGIC;
    tasync_c_q : in STD_LOGIC;
    tasync_d_q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_7 : entity is "tasync";
end tasync_7;

architecture STRUCTURE of tasync_7 is
  signal \^tasync_b_nq\ : STD_LOGIC;
  signal \^tasync_b_q\ : STD_LOGIC;
begin
  tasync_b_nq <= \^tasync_b_nq\;
  tasync_b_q <= \^tasync_b_q\;
\temp_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tasync_b_q\,
      O => \^tasync_b_nq\
    );
temp_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^tasync_b_q\,
      I1 => tasync_a_q,
      I2 => tasync_e_q,
      I3 => tasync_f_q,
      I4 => tasync_c_q,
      I5 => tasync_d_q,
      O => temp_reg_0
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => tasync_b_t,
      CE => '1',
      CLR => btn_cclr,
      D => \^tasync_b_nq\,
      Q => \^tasync_b_q\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_8 is
  port (
    tasync_c_q : out STD_LOGIC;
    tasync_c_nq : out STD_LOGIC;
    tasync_d_t : out STD_LOGIC;
    tasync_g_t : out STD_LOGIC;
    temp_reg_0 : out STD_LOGIC;
    tasync_c_t : in STD_LOGIC;
    btn_cclr : in STD_LOGIC;
    tasync_b_q : in STD_LOGIC;
    tasync_a_q : in STD_LOGIC;
    cclk : in STD_LOGIC;
    tasync_f_q : in STD_LOGIC;
    tasync_e_q : in STD_LOGIC;
    tasync_d_q : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_8 : entity is "tasync";
end tasync_8;

architecture STRUCTURE of tasync_8 is
  signal \^tasync_c_nq\ : STD_LOGIC;
  signal \^tasync_c_q\ : STD_LOGIC;
  signal \^temp_reg_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \temp_i_1__1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \temp_i_2__0\ : label is "soft_lutpair2";
begin
  tasync_c_nq <= \^tasync_c_nq\;
  tasync_c_q <= \^tasync_c_q\;
  temp_reg_0 <= \^temp_reg_0\;
led_rco_OBUF_inst_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^tasync_c_q\,
      I1 => tasync_d_q,
      O => \^temp_reg_0\
    );
\temp_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tasync_c_q\,
      O => \^tasync_c_nq\
    );
\temp_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \^tasync_c_q\,
      I1 => tasync_b_q,
      I2 => tasync_a_q,
      I3 => cclk,
      O => tasync_d_t
    );
\temp_i_2__4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => \^temp_reg_0\,
      I1 => tasync_f_q,
      I2 => tasync_e_q,
      I3 => tasync_a_q,
      I4 => tasync_b_q,
      I5 => cclk,
      O => tasync_g_t
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => tasync_c_t,
      CE => '1',
      CLR => btn_cclr,
      D => \^tasync_c_nq\,
      Q => \^tasync_c_q\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tasync_9 is
  port (
    tasync_d_q : out STD_LOGIC;
    tasync_d_nq : out STD_LOGIC;
    tasync_d_t : in STD_LOGIC;
    btn_cclr : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tasync_9 : entity is "tasync";
end tasync_9;

architecture STRUCTURE of tasync_9 is
  signal \^tasync_d_nq\ : STD_LOGIC;
  signal \^tasync_d_q\ : STD_LOGIC;
begin
  tasync_d_nq <= \^tasync_d_nq\;
  tasync_d_q <= \^tasync_d_q\;
\temp_i_1__2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^tasync_d_q\,
      O => \^tasync_d_nq\
    );
temp_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => tasync_d_t,
      CE => '1',
      CLR => btn_cclr,
      D => \^tasync_d_nq\,
      Q => \^tasync_d_q\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity count8tristate is
  port (
    temp_reg : out STD_LOGIC;
    led_rco_OBUF : out STD_LOGIC;
    led_q_OBUF : out STD_LOGIC_VECTOR ( 7 downto 0 );
    internal_clk : in STD_LOGIC;
    not_rclk : in STD_LOGIC;
    cclk : in STD_LOGIC;
    btn_cclr_IBUF : in STD_LOGIC;
    lopt : in STD_LOGIC
  );
end count8tristate;

architecture STRUCTURE of count8tristate is
  signal tasync_a_n_5 : STD_LOGIC;
  signal tasync_a_nq : STD_LOGIC;
  signal tasync_a_q : STD_LOGIC;
  signal tasync_b_n_2 : STD_LOGIC;
  signal tasync_b_nq : STD_LOGIC;
  signal tasync_b_q : STD_LOGIC;
  signal tasync_b_t : STD_LOGIC;
  signal tasync_c_n_4 : STD_LOGIC;
  signal tasync_c_nq : STD_LOGIC;
  signal tasync_c_q : STD_LOGIC;
  signal tasync_c_t : STD_LOGIC;
  signal tasync_d_nq : STD_LOGIC;
  signal tasync_d_q : STD_LOGIC;
  signal tasync_d_t : STD_LOGIC;
  signal tasync_e_nq : STD_LOGIC;
  signal tasync_e_q : STD_LOGIC;
  signal tasync_e_t : STD_LOGIC;
  signal tasync_f_nq : STD_LOGIC;
  signal tasync_f_q : STD_LOGIC;
  signal tasync_f_t : STD_LOGIC;
  signal tasync_g_nq : STD_LOGIC;
  signal tasync_g_q : STD_LOGIC;
  signal tasync_g_t : STD_LOGIC;
  signal tasync_h_nq : STD_LOGIC;
  signal tasync_h_q : STD_LOGIC;
  signal tasync_h_t : STD_LOGIC;
  signal \^temp_reg\ : STD_LOGIC;
  signal NLW_tasync_a_internal_clk_UNCONNECTED : STD_LOGIC;
begin
  temp_reg <= \^temp_reg\;
rssync_a: entity work.rssync
     port map (
      led_q_OBUF(0) => led_q_OBUF(0),
      not_rclk => not_rclk,
      tasync_a_nq => tasync_a_nq
    );
rssync_b: entity work.rssync_0
     port map (
      led_q_OBUF(0) => led_q_OBUF(1),
      not_rclk => not_rclk,
      tasync_b_nq => tasync_b_nq
    );
rssync_c: entity work.rssync_1
     port map (
      led_q_OBUF(0) => led_q_OBUF(2),
      not_rclk => not_rclk,
      tasync_c_nq => tasync_c_nq
    );
rssync_d: entity work.rssync_2
     port map (
      led_q_OBUF(0) => led_q_OBUF(3),
      not_rclk => not_rclk,
      tasync_d_nq => tasync_d_nq
    );
rssync_e: entity work.rssync_3
     port map (
      led_q_OBUF(0) => led_q_OBUF(4),
      not_rclk => not_rclk,
      tasync_e_nq => tasync_e_nq
    );
rssync_f: entity work.rssync_4
     port map (
      led_q_OBUF(0) => led_q_OBUF(5),
      not_rclk => not_rclk,
      tasync_f_nq => tasync_f_nq
    );
rssync_g: entity work.rssync_5
     port map (
      led_q_OBUF(0) => led_q_OBUF(6),
      not_rclk => not_rclk,
      tasync_g_nq => tasync_g_nq
    );
rssync_h: entity work.rssync_6
     port map (
      led_q_OBUF(0) => led_q_OBUF(7),
      not_rclk => not_rclk,
      tasync_h_nq => tasync_h_nq
    );
tasync_a: entity work.tasync
     port map (
      btn_cclr => \^temp_reg\,
      cclk => cclk,
      internal_clk => NLW_tasync_a_internal_clk_UNCONNECTED,
      led_rco => tasync_a_n_5,
      lopt => lopt,
      tasync_a_nq => tasync_a_nq,
      tasync_a_q => tasync_a_q,
      tasync_b_q => tasync_b_q,
      tasync_b_t => tasync_b_t,
      tasync_c_q => tasync_c_q,
      tasync_c_t => tasync_c_t,
      tasync_d_q => tasync_d_q,
      tasync_e_t => tasync_e_t
    );
tasync_b: entity work.tasync_7
     port map (
      btn_cclr => \^temp_reg\,
      tasync_a_q => tasync_a_q,
      tasync_b_nq => tasync_b_nq,
      tasync_b_q => tasync_b_q,
      tasync_b_t => tasync_b_t,
      tasync_c_q => tasync_c_q,
      tasync_d_q => tasync_d_q,
      tasync_e_q => tasync_e_q,
      tasync_f_q => tasync_f_q,
      temp_reg_0 => tasync_b_n_2
    );
tasync_c: entity work.tasync_8
     port map (
      btn_cclr => \^temp_reg\,
      cclk => cclk,
      tasync_a_q => tasync_a_q,
      tasync_b_q => tasync_b_q,
      tasync_c_nq => tasync_c_nq,
      tasync_c_q => tasync_c_q,
      tasync_c_t => tasync_c_t,
      tasync_d_q => tasync_d_q,
      tasync_d_t => tasync_d_t,
      tasync_e_q => tasync_e_q,
      tasync_f_q => tasync_f_q,
      tasync_g_t => tasync_g_t,
      temp_reg_0 => tasync_c_n_4
    );
tasync_d: entity work.tasync_9
     port map (
      btn_cclr => \^temp_reg\,
      tasync_d_nq => tasync_d_nq,
      tasync_d_q => tasync_d_q,
      tasync_d_t => tasync_d_t
    );
tasync_e: entity work.tasync_10
     port map (
      btn_cclr => \^temp_reg\,
      cclk => cclk,
      tasync_a_q => tasync_a_q,
      tasync_b_q => tasync_b_q,
      tasync_c_q => tasync_c_q,
      tasync_d_q => tasync_d_q,
      tasync_e_nq => tasync_e_nq,
      tasync_e_q => tasync_e_q,
      tasync_e_t => tasync_e_t,
      tasync_f_t => tasync_f_t
    );
tasync_f: entity work.tasync_11
     port map (
      btn_cclr => \^temp_reg\,
      led_rco_OBUF => led_rco_OBUF,
      tasync_e_q => tasync_e_q,
      tasync_f_nq => tasync_f_nq,
      tasync_f_q => tasync_f_q,
      tasync_f_t => tasync_f_t,
      tasync_g_q => tasync_g_q,
      tasync_h_q => tasync_h_q,
      temp_reg_0 => tasync_c_n_4,
      temp_reg_1 => tasync_a_n_5
    );
tasync_g: entity work.tasync_12
     port map (
      btn_cclr_IBUF => btn_cclr_IBUF,
      cclk => cclk,
      tasync_g_nq => tasync_g_nq,
      tasync_g_q => tasync_g_q,
      tasync_g_t => tasync_g_t,
      tasync_h_t => tasync_h_t,
      temp_reg_0 => \^temp_reg\,
      temp_reg_1 => tasync_b_n_2
    );
tasync_h: entity work.tasync_13
     port map (
      btn_cclr => \^temp_reg\,
      tasync_h_nq => tasync_h_nq,
      tasync_h_q => tasync_h_q,
      tasync_h_t => tasync_h_t
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity freqdiv is
  port (
    cclk : out STD_LOGIC;
    not_rclk : out STD_LOGIC;
    prediv_clk_BUFG : in STD_LOGIC;
    btn_cclr : in STD_LOGIC;
    internal_clk : in STD_LOGIC;
    btn_rclk_IBUF : in STD_LOGIC
  );
end freqdiv;

architecture STRUCTURE of freqdiv is
  signal div_2 : STD_LOGIC;
  signal div_4 : STD_LOGIC;
begin
tasync_2: entity work.tasync_14
     port map (
      btn_cclr => btn_cclr,
      div_2 => div_2,
      prediv_clk_BUFG => prediv_clk_BUFG
    );
tasync_4: entity work.tasync_15
     port map (
      btn_cclr => btn_cclr,
      div_2 => div_2,
      div_4 => div_4
    );
tasync_8: entity work.tasync_16
     port map (
      btn_cclr => btn_cclr,
      btn_rclk_IBUF => btn_rclk_IBUF,
      cclk => cclk,
      div_4 => div_4,
      internal_clk => internal_clk,
      not_rclk => not_rclk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity dev_wrapper is
  port (
    sysclk_p : in STD_LOGIC;
    sysclk_n : in STD_LOGIC;
    btn_oe : in STD_LOGIC;
    btn_rclk : in STD_LOGIC;
    btn_ccken : in STD_LOGIC;
    btn_cclr : in STD_LOGIC;
    led_q : out STD_LOGIC_VECTOR ( 7 downto 0 );
    led_rco : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of dev_wrapper : entity is true;
end dev_wrapper;

architecture STRUCTURE of dev_wrapper is
  signal btn_ccken_IBUF : STD_LOGIC;
  signal btn_cclr_IBUF : STD_LOGIC;
  signal btn_rclk_IBUF : STD_LOGIC;
  signal cclk : STD_LOGIC;
  signal counter_n_0 : STD_LOGIC;
  signal internal_clk : STD_LOGIC;
  signal led_q_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \led_q_TRI[0]\ : STD_LOGIC;
  signal led_rco_OBUF : STD_LOGIC;
  signal not_rclk : STD_LOGIC;
  signal prediv_clk : STD_LOGIC;
  signal prediv_clk_BUFG : STD_LOGIC;
  signal NLW_counter_internal_clk_UNCONNECTED : STD_LOGIC;
  attribute OPT_INSERTED : boolean;
  attribute OPT_INSERTED of btn_ccken_IBUF_inst : label is std.standard.true;
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clk_ibufds : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clk_ibufds : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clk_ibufds : label is "AUTO";
  attribute box_type : string;
  attribute box_type of clk_ibufds : label is "PRIMITIVE";
begin
btn_ccken_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => btn_ccken,
      O => btn_ccken_IBUF
    );
btn_cclr_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => btn_cclr,
      O => btn_cclr_IBUF
    );
btn_oe_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => btn_oe,
      O => \led_q_TRI[0]\
    );
btn_rclk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => btn_rclk,
      O => btn_rclk_IBUF
    );
clk_div: entity work.freqdiv
     port map (
      btn_cclr => counter_n_0,
      btn_rclk_IBUF => btn_rclk_IBUF,
      cclk => cclk,
      internal_clk => internal_clk,
      not_rclk => not_rclk,
      prediv_clk_BUFG => prediv_clk_BUFG
    );
clk_ibufds: unisim.vcomponents.IBUFDS
    generic map(
      DQS_BIAS => "FALSE",
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => sysclk_p,
      IB => sysclk_n,
      O => prediv_clk
    );
counter: entity work.count8tristate
     port map (
      btn_cclr_IBUF => btn_cclr_IBUF,
      cclk => cclk,
      internal_clk => NLW_counter_internal_clk_UNCONNECTED,
      led_q_OBUF(7 downto 0) => led_q_OBUF(7 downto 0),
      led_rco_OBUF => led_rco_OBUF,
      lopt => cclk,
      not_rclk => not_rclk,
      temp_reg => counter_n_0
    );
\led_q_OBUFT[0]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => led_q_OBUF(0),
      O => led_q(0),
      T => \led_q_TRI[0]\
    );
\led_q_OBUFT[1]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => led_q_OBUF(1),
      O => led_q(1),
      T => \led_q_TRI[0]\
    );
\led_q_OBUFT[2]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => led_q_OBUF(2),
      O => led_q(2),
      T => \led_q_TRI[0]\
    );
\led_q_OBUFT[3]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => led_q_OBUF(3),
      O => led_q(3),
      T => \led_q_TRI[0]\
    );
\led_q_OBUFT[4]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => led_q_OBUF(4),
      O => led_q(4),
      T => \led_q_TRI[0]\
    );
\led_q_OBUFT[5]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => led_q_OBUF(5),
      O => led_q(5),
      T => \led_q_TRI[0]\
    );
\led_q_OBUFT[6]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => led_q_OBUF(6),
      O => led_q(6),
      T => \led_q_TRI[0]\
    );
\led_q_OBUFT[7]_inst\: unisim.vcomponents.OBUFT
     port map (
      I => led_q_OBUF(7),
      O => led_q(7),
      T => \led_q_TRI[0]\
    );
led_rco_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => led_rco_OBUF,
      O => led_rco
    );
prediv_clk_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => prediv_clk,
      O => prediv_clk_BUFG
    );
temp_reg_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cclk,
      O => internal_clk
    );
end STRUCTURE;
