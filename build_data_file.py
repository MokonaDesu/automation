#!/usr/bin/python

def zero_pad(bin_value, size):
    if (len(bin_value) < size):
        return '0' * (size - len(bin_value)) + bin_value
    return bin_value

for x in range(0,16):
    for y in range(0, 16):
        for c in range(0, 2):
            print zero_pad(bin(c)[2:], 1)
            print zero_pad(bin(x)[2:], 4)
            print zero_pad(bin(y)[2:], 4)
            print zero_pad(bin(x + y + c)[2:], 5)
